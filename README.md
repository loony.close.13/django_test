# Django + RESTapi + JWT тестовый проект.

Даный проект реализован в плане понимания работы включённых в него технологий, используется Dgango framework и реализован на его основе:
- REST api — архитектурный стиль взаимодействия компонентов распределённого приложения в сети.
- JSON Web Token(jwt) — это открытый стандарт для создания токенов доступа, основанный на формате JSON. Как правило, используется для передачи данных для аутентификации в клиент-серверных приложениях.



### Как запустить:
- Doker
- docker-compose

### Чтобы запустить проект 
`docker-compose up`
в папке с проектом

### После пройти по сылке: 
- для регистрации нового пользователя [localhost:8000/account/api/register](http://localhost:8000/account/api/register)
- для получения токена авторизации [localhost:8000/api/token/](http://localhost:8000/api/token/)

