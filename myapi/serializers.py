from rest_framework import serializers

from .models import NewUser

class NewUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = NewUser
        fields = ('login', 'email')