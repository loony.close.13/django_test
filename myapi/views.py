from django.shortcuts import render

from rest_framework import viewsets

from .serializers import NewUserSerializer
from .models import NewUser

class NewUserViewSet(viewsets.ModelViewSet):
    queryset = NewUser.objects.all().order_by('login')
    serializer_class = NewUserSerializer
