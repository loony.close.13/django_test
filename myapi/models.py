from django.db import models

class NewUser(models.Model):
    login = models.CharField(max_length=16)
    email = models.CharField(max_length=60)

    def __str__(self):
        return self.login