FROM python:3.9-alpine
RUN pip install pipenv
RUN mkdir /code
COPY . /code
WORKDIR /code
RUN pipenv install